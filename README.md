## Reconify Command Line Interface

### Getting started

* Scan for details

```bash
python reconify.py --details --input=<url>
```

* Scan for subdomains

```bash
python reconify.py --subdomains --input=<url>
```

* Scan for urls

```bash
python reconify.py --urls --input=<url>
```

* Scan for whois

```bash
python reconify.py --whois --input=<url>
```

* Scan for headers

```bash
python reconify.py --headers --input=<url>
```

* Scan for stack

```bash
python reconify.py --stack --input=<url>
```

* Scan for tlsinfo

```bash
python reconify.py --tlsinfo --input=<url>
```

* Scan for tlsvuln

```bash
python reconify.py --tlsvuln --input=<url>
```

* You can also combine multiple options

```bash
python reconify.py --tlsinfo --tlsvuln --input=<url>
```


