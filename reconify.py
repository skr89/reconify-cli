"""Reconify.

Usage: 
    reconify.py [options] --input=<url> 

Options:
    --details
    --urls
    --subdomains
    --ports
    --whois
    --headers
    --stack
    --tlsinfo
    --tlsvuln
"""

from docopt import docopt
from beautifultable import BeautifulTable
from termcolor import colored
import requests
import validators

BASE_URL = "https://khrvm9a6j5.execute-api.ap-southeast-1.amazonaws.com/dev"   

def handle_request(request_url,input_url):
    data = {
        "url":input_url
    }
    r = requests.post(request_url,json=data)
    if r.status_code == 200:
        return r.json()
    else:
        return {}

def handle_urls(json_data):
    table = BeautifulTable() 
    table.columns.header = [
        colored("URL","green")
    ]           
    for u in json_data:
        table.rows.append([u])
    print(table)

def handle_stack(json_data):
    table = BeautifulTable()                
    for u in json_data.get('tech_stack'):
        table.rows.append(
            [
                colored(u.get('category'),"green"),
                u.get('technology')
            ]
        )
    print(table)

def handle_tlsvuln(json_data):
    table = BeautifulTable()                
    for u in json_data:
        for k,v in u.items():        
            table.rows.append(
                [
                    colored(k,"green"),
                    colored(v,"red") if v == "Vulnerable" else v
                ]
            )
    print(table)    

def handle_headers(json_data):
    table = BeautifulTable() 
    table.columns.header = [
        colored("Headers","green")
    ]           
    table.columns.alignment[colored("Headers","green")] = BeautifulTable.ALIGN_LEFT
    table.rows.append(['\n'.join(json_data)])    
    print(table)    

def handle_subdomains(json_data):
    table = BeautifulTable() 
    table.columns.header = [
        colored("Domain","green"),
        colored("IP","green"),
    ]           
    for u in json_data.get('data',[]):
        table.rows.append([
            u.get('Domain'),
            u.get('ip_info'),
        ])
    print(table)

def handle_tlsinfo(json_data):
    table = BeautifulTable() 
    table.columns.header = [
        colored("Enabled","green"),
        colored("Not Enabled","green"),
    ]    
    u = json_data.get('protocols',{})            
    table.rows.append([
        '\n'.join(u.get('enabled')),
        '\n'.join(u.get('notEnabled')),
    ])
    print(table)    

def handle_whois(json_data):
    table = BeautifulTable() 
    table.columns.header = [
        colored("Description","green"),
        colored("Expiry","green"),
        colored("Organization","green"),
    ]           
    table.rows.append([
        json_data.get('Description'),
        json_data.get('expiration_date'),
        json_data.get('org'),
    ])    
    print(table)    

def handle_ports(json_data):
    table = BeautifulTable() 
    table.columns.header = [
        colored("Port","green"),
        colored("Protocol","green"),
        colored("Service","green"),
        colored("Version","green"),
        colored("State","green"),
    ]           
    for u in json_data:
        table.rows.append([
            u.get('port'),
            u.get('protocol'),
            u.get('service'),
            u.get('service_version'),            
            colored(u.get('state'),"red") if u.get('state') == "open" else u.get('state')
        ])
    print(table)    

def handle_details(json_data):
    table = BeautifulTable()     
    table.columns.header = [
        colored("Domain","green"),
        colored("Expiry","green"),
        colored("Organization","green"),
        colored("Server","green")
    ]             
    table.rows.append([        
        json_data.get('domain'),
        json_data.get('expiration_date'),              
        json_data.get('org'),
        json_data.get('server'),
    ])    
    print(table)
    table = BeautifulTable()     
    table.columns.header = [
        colored("Canonical Names","green"),
        colored("Load Balancer IP's","green")
    ]             
    table.rows.append([
        '\n'.join(json_data.get('canonical_names')),        
        '\n'.join(json_data.get('load_balancer_ips')),                
    ])    
    print(table)
    table = BeautifulTable()     
    table.columns.header = [
        colored("Mail Servers","green"),
        colored("Name Servers","green")
    ]         
    table.rows.append([        
        '\n'.join(json_data.get('mail_servers')),
        '\n'.join(json_data.get('name_servers')),                
    ])    
    print(table)
    table = BeautifulTable()     
    table.columns.header = [
        colored("SOA","green"),
        colored("TXT","green")
    ]         
    table.rows.append([                       
        '\n'.join(json_data.get('soa')),
        '\n'.join(json_data.get('txt')),
    ])    
    print(table)

def scan(args):    
    input_url = args['--input']     
    if validators.url(input_url):
        if args['--details']:
            request_url = "{0}/details".format(BASE_URL)  
            result = handle_request(request_url,input_url)        
            handle_details(result)
        if args['--urls']:
            request_url = "{0}/urls".format(BASE_URL)  
            input_url = args['--input']  
            result = handle_request(request_url,input_url)                                                        
            handle_urls(result)
        if args['--subdomains']:
            request_url = "{0}/subdomains".format(BASE_URL)  
            input_url = args['--input']  
            result = handle_request(request_url,input_url)                                                                
            handle_subdomains(result)
        if args['--ports']:
            request_url = "{0}/ports".format(BASE_URL)  
            input_url = args['--input']  
            result = handle_request(request_url,input_url)                                                                        
            handle_ports(result)
        if args['--whois']:
            request_url = "{0}/whois".format(BASE_URL)  
            input_url = args['--input']  
            result = handle_request(request_url,input_url)                                                                        
            handle_whois(result)
        if args['--headers']:
            request_url = "{0}/headers".format(BASE_URL)  
            input_url = args['--input']  
            result = handle_request(request_url,input_url)                                                                        
            handle_headers(result)
        if args['--stack']:
            request_url = "{0}/stack".format(BASE_URL)  
            input_url = args['--input']  
            result = handle_request(request_url,input_url)                                                                        
            handle_stack(result)
        if args['--tlsinfo']:
            request_url = "{0}/tlsinfo".format(BASE_URL)  
            input_url = args['--input']  
            result = handle_request(request_url,input_url)                                                                        
            handle_tlsinfo(result)
        if args['--tlsvuln']:
            request_url = "{0}/tlsvuln".format(BASE_URL)  
            input_url = args['--input']  
            result = handle_request(request_url,input_url)                                                                        
            handle_tlsvuln(result)
    else:
        print('Invalid URL')        


if __name__ == '__main__':
    args = docopt(__doc__)    
    if args['--input']:
        scan(args)                 
    